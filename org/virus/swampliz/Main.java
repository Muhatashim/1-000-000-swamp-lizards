package org.virus.swampliz;

import org.virus.swampliz.core.TaskConsumer;
import org.virus.swampliz.core.TaskProducer;
import org.virus.swampliz.core.impl.rt6.WalkTask;
import org.virus.swampliz.rtimpl.Rt4Delegate;
import org.virus.swampliz.rtimpl.Rt6Delegate;

import javax.swing.*;
import java.util.Collection;
import java.util.concurrent.*;
import java.util.logging.Level;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/29/2014
 * Time: 3:33 PM
 */
public class Main {
    public static final String SCRIPT_NAME        = "1,000,000 Green Swamp Lizards";
    public static final String SCRIPT_DESCRIPTION = "Collects green swamp lizards and banks them";

    /**
     * List of executions or instructions to follow
     */
    private          BlockingQueue<Runnable> executions; //LinkedTransferQueue is being extremely weird for now....
    private volatile boolean                 keepRunning;
    private          ExecutorService         executor;

    private static Main       instance;
    private        RTDelegate script;

    public Main(RTDelegate script) {
        this.script = script;
        if (script instanceof Rt4Delegate) {
            stopScript("Old School Unsupported");
            return;
        }

        executions = new LinkedTransferQueue<Runnable>();
        keepRunning = true;

        //only supporting rt6 atm, dunno how it's for rt4. Maybe I'll code it later
        org.powerbot.script.rt6.ClientContext ctx = ((Rt6Delegate) script).getCTX();

        executor = Executors.newFixedThreadPool(4);
        executor.execute(new TaskProducer(ctx,
                new WalkTask(ctx)));
        executor.execute(new TaskConsumer(false));
        executor.execute(new TaskConsumer(false));
//        executor.execute(new TaskConsumer(true));
        executor.execute(new ShutdownCheck());

        synchronized (Main.class) {
            instance = this;
            Main.class.notifyAll();
        }
    }

    public BlockingQueue<Runnable> getExecutions() {
        return executions;
    }

    public RTDelegate getScript() {
        return script;
    }

    /**
     * Waits until the instance of this class has been made or generates a fatal error message
     *
     * @return Instance of the main class
     */
    public static Main get() {
        if (instance == null) {
            try {
                synchronized (Main.class) {
                    if (instance == null)
                        Main.class.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                fatalError("Could not create an instance of the main script class");
            }
        }

        return instance;
    }

    public static void fatalError(String lastWords) {
        JOptionPane.showMessageDialog(null, lastWords + "\nIf this problem persists, please contact the developer.");
    }

    public void stopScript() {
        stopScript(null);
    }

    public void stopScript(String message) {
        if (message != null)
            script.getLogger().log(Level.INFO, message);
        script.getLogger().log(Level.INFO, "Shutting down script.");
        keepRunning = false;
        script.getCTX().controller.stop();
        try {
            if (executor.awaitTermination(7, TimeUnit.SECONDS))
                script.getLogger().log(Level.INFO, "Script shut down successfully.");
            else
                script.getLogger().log(Level.INFO, "Script could not shut down in 7 seconds.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tries to transfer the task within 4 seconds, if it can't the task is never run
     */
    public void submit(Runnable... runnables) {
        for (Runnable runnable : runnables)
            try {
//                executions.transfer(runnable);
                executions.offer(runnable, 4, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    /**
     * Tries to transfer the task within 4 seconds, if it can't the task is never run
     */
    public void submit(Collection<Runnable> runnables) {
        for (Runnable runnable : runnables)
            try {
//                executions.transfer(runnable);
                executions.offer(runnable, 4, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }

    public Runnable nextTask() {
        try {
            return executions.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return executions.poll();
    }

    public void clearExecutionQueue() {
        script.getLogger().log(Level.INFO, "Something caused the script to interrupt its current execution queue");
        executions.clear();
    }

    public boolean keepRunning() {
        return keepRunning;
    }
}
