package org.virus.swampliz.rtimpl;

import org.powerbot.script.Script;
import org.powerbot.script.rt6.ClientContext;
import org.powerbot.script.AbstractScript;
import org.virus.swampliz.Main;
import org.virus.swampliz.RTDelegate;

import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/30/2014
 * Time: 4:21 PM
 */
@Script.Manifest(name = Main.SCRIPT_NAME, description = "RS3 implementation the script. " +
        Main.SCRIPT_DESCRIPTION)
public class Rt6Delegate extends AbstractScript<ClientContext> implements RTDelegate<ClientContext> {

    public Rt6Delegate() {
        new Main(this);
    }

    public ClientContext getCTX() {
        return ctx;
    }

    public Properties getSettings() {
        return settings;
    }

    public Logger getLogger() {
        return log;
    }
}
