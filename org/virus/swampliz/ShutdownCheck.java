package org.virus.swampliz;

import org.powerbot.script.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/31/2014
 * Time: 1:49 PM
 */
public class ShutdownCheck implements Runnable {
    @Override
    public void run() {
        while (!Main.get().getScript().getCTX().controller.isStopping())
            try {
                Thread.sleep(Random.nextInt(100, 400));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        Main.get().stopScript();
    }
}
