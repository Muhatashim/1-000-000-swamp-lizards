package org.virus.swampliz.core;

import org.powerbot.script.ClientAccessor;
import org.powerbot.script.ClientContext;
import org.virus.swampliz.Main;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/29/2014
 * Time: 4:42 PM
 */
public abstract class AbstractTask<C extends ClientContext> extends ClientAccessor<C> implements Condition, Runnable {

    private volatile boolean taskSubmitted;
    private          long    maxExecutionTime;
    private          long    startTime;

    public AbstractTask(C c) {
        super(c);
        maxExecutionTime = 5000;
    }

    /**
     * More than one instruction is required for this task
     */
    protected List<Runnable> generateInstructions() {
        return null;
    }

    /**
     * Only one instruction is required for this task
     */
    protected void doInstruction() {
    }

    @Override
    public final void run() {
        startTime = System.currentTimeMillis();
        List<Runnable> instructions;
        if ((instructions = generateInstructions()) != null)
            Main.get().submit(instructions);
        else
            doInstruction();
    }

    protected boolean isTaskSubmitted() {
        return taskSubmitted;
    }

    protected synchronized void setTaskSubmitted(boolean taskSubmitted) {
        this.taskSubmitted = taskSubmitted;
    }

    public boolean isOutOfTime() {
        return System.currentTimeMillis() - startTime > maxExecutionTime;
    }

    public long getMaxExecutionTime() {
        return maxExecutionTime;
    }

    public void setMaxExecutionTime(long maxExecutionTime) {
        this.maxExecutionTime = maxExecutionTime;
    }
}
