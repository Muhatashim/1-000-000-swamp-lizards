package org.virus.swampliz.core.impl.rt6;

import org.powerbot.script.Area;
import org.powerbot.script.Tile;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 1/1/2015
 * Time: 3:06 PM
 */
public class Constants {

    public static final int  GREEN_LIZARDS = 10149;
    public static final int  ROPE          = 954;
    public static final int  NET           = 303;
    public static final Area SWAMP_AREA    = new Area(new Tile(3534, 3454), new Tile(3542,3445));
}
