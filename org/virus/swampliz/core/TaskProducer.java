package org.virus.swampliz.core;

import org.powerbot.script.ClientContext;
import org.powerbot.script.Random;
import org.virus.swampliz.Main;

/**
 * Generates a task with a set of instruction(s) to follow and submits it to the transfer class
 * <p/>
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/29/2014
 * Time: 6:05 PM
 */
public class TaskProducer implements Runnable {

    private ClientContext  ctx;
    private AbstractTask[] tasks;

    public TaskProducer(ClientContext ctx, AbstractTask... tasks) {
        this.ctx = ctx;
        this.tasks = tasks;
    }

    @Override
    public void run() {
        while (Main.get().keepRunning()) {
            for (AbstractTask task : tasks) {
                if (task.checkCondition()) {
                   if (task instanceof SingleInstanceTask && !task.isTaskSubmitted() || task.isOutOfTime() || !(task instanceof SingleInstanceTask)) {
                       Main.get().submit(task);
                        task.setTaskSubmitted(true);
                    }
                }
            }

            try {
                Thread.sleep(Random.nextInt(100, 400));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
