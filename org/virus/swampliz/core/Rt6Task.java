package org.virus.swampliz.core;

import org.powerbot.script.rt6.ClientContext;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/30/2014
 * Time: 5:55 PM
 */
public abstract class Rt6Task extends AbstractTask<ClientContext> {
    public Rt6Task(ClientContext clientContext) {
        super(clientContext);
    }
}
