package org.virus.swampliz.core;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/30/2014
 * Time: 2:18 PM
 */
public interface Condition {

    /**
     * Tests a condition and returns the value
     * @return output of the test
     */
    boolean checkCondition();
}
