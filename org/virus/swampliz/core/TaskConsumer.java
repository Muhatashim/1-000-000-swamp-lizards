package org.virus.swampliz.core;

import org.powerbot.script.Random;
import org.virus.swampliz.Main;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/29/2014
 * Time: 6:26 PM
 */
public class TaskConsumer implements Runnable {

    private boolean synchronousTasks;

    public TaskConsumer(boolean synchronousTasks) {
        this.synchronousTasks = synchronousTasks;
    }

    @Override
    public void run() {
        while (Main.get().keepRunning()) {
            Runnable runnable = Main.get().nextTask();

            if (runnable != null && (synchronousTasks == runnable instanceof SynchronousTask)) {
                runnable.run();
                if (runnable instanceof AbstractTask)
                    ((AbstractTask) runnable).setTaskSubmitted(false);
            }

            try {
                Thread.sleep(Random.nextInt(100, 400));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
