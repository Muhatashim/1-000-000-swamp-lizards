package org.virus.swampliz;

import org.virus.swampliz.core.Condition;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/29/2014
 * Time: 10:47 PM
 */
public class Utils {

    /**
     * Invokes <code>condition.checkCondition()</code> every 100 milliseconds until the method evaluates to true for
     * one second.
     *
     * @param condition The condition to be tested
     * @return <code>true</code> if <code>condition.checkCondition()</code> is true within one second,
     * <code>false</code> otherwise
     * @throws InterruptedException If the thread checking the condition is interrupted before it could finish
     * @see org.virus.swampliz.core.Condition
     */
    public static boolean waitFor(final Condition condition) throws InterruptedException {
        return waitFor(condition, 1, TimeUnit.SECONDS);
    }

    /**
     * Invokes <code>condition.checkCondition()</code> every 100 milliseconds until the method evaluates to true for
     * a specified maximum wait time limit in milliseconds.
     *
     * @param condition The condition to be tested
     * @param waitTime  The maximum wait time in milliseconds before returning false indicating that the condition was
     *                  not met within the specified time
     * @return <code>true</code> if <code>condition.checkCondition()</code> is true within the specified wait time,
     * <code>false</code> otherwise
     * @throws InterruptedException If the thread checking the condition is interrupted before it could finish
     * @see org.virus.swampliz.core.Condition
     */
    public static boolean waitFor(final Condition condition, long waitTime) throws InterruptedException {
        return waitFor(condition, waitTime, TimeUnit.MILLISECONDS);
    }

    /**
     * Invokes <code>condition.checkCondition()</code> every 100 milliseconds until the method evaluates to true for
     * a specified maximum wait time limit.
     *
     * @param condition The condition to be tested
     * @param waitTime  The maximum wait time before returning false indicating that the condition was not met within
     *                  the specified time
     * @param timeUnit The <code>TimeUnit</code> to use
     * @return <code>true</code> if <code>condition.checkCondition()</code> is true within the specified wait time,
     * <code>false</code> otherwise
     * @throws InterruptedException If the thread checking the condition is interrupted before it could finish
     * @see org.virus.swampliz.core.Condition
     * @see java.util.concurrent.TimeUnit
     */
    public static boolean waitFor(final Condition condition, long waitTime, TimeUnit timeUnit) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    while (!condition.checkCondition())
                        Thread.sleep(100);
                    latch.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return latch.await(1, TimeUnit.SECONDS);
    }
}
