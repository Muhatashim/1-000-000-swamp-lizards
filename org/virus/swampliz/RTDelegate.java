package org.virus.swampliz;

import org.powerbot.script.ClientContext;

import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Muhatashim
 * Date: 12/30/2014
 * Time: 4:22 PM
 */
public interface RTDelegate<T extends ClientContext> {
    T getCTX();

    Properties getSettings();

    Logger getLogger();
}
